/*
 * Copyright (C) 2020 Succboy6000 (Thanh Vinh Nguyen).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.	If not, see <https://www.gnu.org/licenses/>.
 */

/* FUSION WINDOW MANAGER */

#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <framebuffer.h>
#include <mouse.h>

fb_device_t zbuf; // z-buffer (aka back buffer)

#define BGCOLOR					  rgb(127, 127, 127) // background color
#define PTRCOLOR					rgb( 0 ,	0 ,	 0 ) // mouse pointer color
#define PTRCOLOR_L				rgb(255,  0 ,	 0 ) // mouse pointer color (when left mouse button is pressed)
#define PTRCOLOR_R				rgb( 0 , 255,	 0 ) // mouse pointer color (when right mouse button is pressed)
#define DRAGCOLOR         rgb(255, 255, 255) // drag border color

/* 16x16 mouse cursor */
uint16_t cursor[16] = {
  0b1111111111111111,
  0b0111111111111111,
  0b0011111111111111,
  0b0001111111111111,
  0b0000111111111111,
  0b0000011111111111,
  0b0000001111111111,
  0b0000011111111111,
  0b0000111111111111,
  0b0001111111111111,
  0b0011111110111111,
  0b0111111100011111,
  0b1111111000001111,
  0b1111110000000111,
  0b1111100000000011,
  0b1111000000000001
};

void update_fb(void) {
	memcpy(fb_default->buffer, zbuf.buffer, zbuf.height * zbuf.pitch);
}

int main() {
	/* create z-buffer */
	memcpy(&zbuf, fb_default, sizeof(fb_device_t));
	zbuf.buffer = malloc(zbuf.height * zbuf.pitch);
	/* reset pointer */
	mouse_x = zbuf.width / 2; mouse_y = zbuf.height / 2;
  /* do what a WM is supposed to do I guess */
  uint8_t old_stat = 0, old_x = 0, old_y = 0;
	while(1) {
		fb_fill(&zbuf, BGCOLOR);
		uint32_t ptrc = PTRCOLOR;
		if(mouse_buttons & MOUSE_RIGHT) ptrc = PTRCOLOR_R;
    if(mouse_buttons & MOUSE_LEFT) {
      if(!(old_stat & MOUSE_LEFT)) {
        old_x = mouse_x; old_y = mouse_y;
      }
      ptrc = PTRCOLOR_L;
      fb_drawrect(&zbuf, old_x, old_y, mouse_x, mouse_y, DRAGCOLOR);
    }
    old_stat = mouse_buttons;
		for(uint8_t y = 0; y < 16; y++) {
      for(uint8_t x = 0; x < 16; x++) {
        if(cursor[y] & (1 << x)) fb_putpixel(&zbuf, mouse_x + x, mouse_y + y, ptrc);
      }
    }
		update_fb();
	}
	return 0;
}
