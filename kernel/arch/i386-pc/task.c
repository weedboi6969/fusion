/*
 * Copyright (C) 2020 Succboy6000 (Thanh Vinh Nguyen).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <task.h>
#include <stdlib.h>
#include <paging.h>
#include <timer.h>
#include <textmode.h>
#include <i386-pc/textmode.h>
#include <stdio.h>

extern uint8_t text_attribs;
extern uint32_t text_bg, text_fg, text_x, text_y;
uint8_t task_pause = 0; // set to 1 to pause yielding (used for stall functions)

typedef struct {
	uint32_t eax, ebx, ecx, edx, esi, edi, esp, ebp, eip, eflags, cr3;
} __attribute__ ((packed)) task_regs_t;

typedef struct task {
	uint32_t stack[1024];
	task_regs_t regs;
	text_data_t tdat;
	struct task *next;
} __attribute__ ((packed)) task_t;

uintptr_t task_running = 0;
task_t task_kernel_struct;
uintptr_t task_kernel = (uintptr_t) &task_kernel_struct;

/* find the preceding task */
static task_t *task_before(task_t *task) {
	task_t *ret = &task_kernel_struct;
	while(1) {
		if((uintptr_t) ret->next == (uintptr_t) task) return ret;
		ret = (task_t*) ret->next;
	}
}

/* find the last task in the queue */
static task_t *task_last(void) {
	return task_before(&task_kernel_struct);
}

uintptr_t task_create(void (*main)(), uint32_t flags, uintptr_t pstruct) {
	(void) pstruct; // not touching it for now
	task_t *task = malloc_ext(sizeof(task_t), 4096, NULL);
	task->regs.eax = 0;
	task->regs.ebx = 0;
	task->regs.ecx = 0;
	task->regs.edx = 0;
	task->regs.esi = 0;
	task->regs.edi = 0;
	task->regs.eflags = (flags == 0) ? task_kernel_struct.regs.eflags : flags;
	task->regs.eip = (uintptr_t) main;
	task->regs.cr3 = task_kernel_struct.regs.cr3;
	task->regs.esp = (uintptr_t) &task->stack + 4092;
	task->stack[1023] = (uintptr_t) &exit; // exit function in stdlib.h
	text_data_save(&task->tdat);
	if(!task_pid_alloc((uintptr_t) task)) {
		free(task);
		return 0;
	}
	task_t *last = task_last(); // find the final task on the queue
	task->next = last->next; // transfer the succeeding task over
	last->next = (struct task*) task; // put the new task at the end of the queue
	return (uintptr_t) task;
}

void task_remove(uintptr_t task) {
	task_t *task_struct = (task_t*) task;
	task_before(task_struct)->next = task_struct->next;
	free((void*) task); // free the memory allocated by the task
}

extern void task_switchreg(task_regs_t *src, task_regs_t *dest); // in task_asm.s

void task_switch(uintptr_t task) {
	task_t *task_running_struct = (task_t*) task_running;
	task_t *last = task_running_struct;
	task_running_struct = (task_t*) task;
	task_running = (uintptr_t) task_running_struct;
	text_data_save(&last->tdat); text_data_load(&task_running_struct->tdat);
	// printf("yielding\n");
	task_switchreg((task_regs_t*) &last->regs, (task_regs_t*) &task_running_struct->regs); // switch registers over
}

void task_yield(void) {
	if(task_pause) return;
	task_t *task_running_struct = (task_t*) task_running;
	// printf("yielding to pid %u\n", task_task2pid((uintptr_t) task_running_struct->next));
	task_switch((uintptr_t) task_running_struct->next);
}

void task_init(void) {
	/* put eflags and cr3 into kernel task */
	asm volatile("movl %%cr3, %%eax; mov %%eax, %0;" : "=m"(task_kernel_struct.regs.cr3) : : "%eax");
	asm volatile("pushfl; movl (%%esp), %%eax; movl %%eax, %0; popfl;" : "=m"(task_kernel_struct.regs.eflags) : : "%eax");
	text_data_save(&task_kernel_struct.tdat);
	task_kernel_struct.next = (struct task*) &task_kernel_struct; // so that if we yield we don't end up jumping into nowhere
	task_pidmap[0] = task_kernel; // kernel gets PID 0
	for(uint32_t i = 1; i < TASK_MAXPROCS; i++) task_pidmap[i] = 0;
	task_running = task_kernel; // because the current task we're running is the kernel task
	timer_add_event(TIMER_PERIODIC, task_yield, 1000);
}
