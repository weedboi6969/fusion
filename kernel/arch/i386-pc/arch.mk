KERNEL_ARCH_CRT_OBJS=\
$(ARCHDIR)/crti.o \
$(ARCHDIR)/crtbegin.o \
$(ARCHDIR)/crtend.o \
$(ARCHDIR)/crtn.o

KERNEL_ARCH_OBJS=\
$(ARCHDIR)/boot.o \
$(ARCHDIR)/io.o \
$(ARCHDIR)/textmode.o \
$(ARCHDIR)/gdt.o \
$(ARCHDIR)/idt.o \
$(ARCHDIR)/idt_helper.o \
$(ARCHDIR)/exceptions.o \
$(ARCHDIR)/exchelper.o \
$(ARCHDIR)/pic.o \
$(ARCHDIR)/pit.o \
$(ARCHDIR)/paging.o \
$(ARCHDIR)/paging_asm.o \
$(ARCHDIR)/cmos.o \
$(ARCHDIR)/rtc.o \
$(ARCHDIR)/int32.o \
$(ARCHDIR)/vbe.o \
$(ARCHDIR)/multiboot.o \
$(ARCHDIR)/ps2.o \
$(ARCHDIR)/beep.o \
$(ARCHDIR)/task.o \
$(ARCHDIR)/task_asm.o \
$(ARCHDIR)/dma.o \
$(ARCHDIR)/pci.o \
$(ARCHDIR)/acpi.o \
$(ARCHDIR)/power.o \
$(ARCHDIR)/laihost.o \
$(ARCHDIR)/string_asm.o \
$(ARCHDIR)/string.o \
$(ARCHDIR)/stdlib_asm.o \
$(ARCHDIR)/uart.o \
$(ARCHDIR)/mmap.o \
$(ARCHDIR)/lai/core/error.o \
$(ARCHDIR)/lai/core/eval.o \
$(ARCHDIR)/lai/core/exec-operand.o \
$(ARCHDIR)/lai/core/exec.o \
$(ARCHDIR)/lai/core/libc.o \
$(ARCHDIR)/lai/core/ns.o \
$(ARCHDIR)/lai/core/object.o \
$(ARCHDIR)/lai/core/opregion.o \
$(ARCHDIR)/lai/core/os_methods.o \
$(ARCHDIR)/lai/core/variable.o \
$(ARCHDIR)/lai/core/vsnprintf.o \
$(ARCHDIR)/lai/drivers/ec.o \
$(ARCHDIR)/lai/drivers/timer.o \
$(ARCHDIR)/lai/helpers/pc-bios.o \
$(ARCHDIR)/lai/helpers/pci.o \
$(ARCHDIR)/lai/helpers/pm.o \
$(ARCHDIR)/lai/helpers/resource.o \
$(ARCHDIR)/lai/helpers/sci.o

KERNEL_OBJS=\
kernel/i386-pc.o
