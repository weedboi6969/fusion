/*
 * Copyright (C) 2020 Succboy6000 (Thanh Vinh Nguyen).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <i386-pc/ps2.h>
#include <i386-pc/pic.h>
#include <io.h>
#include <stdio.h>
#include <keyboard.h>
#include <mouse.h>
#include <timer.h>

#define DEBUG

ps2_port_t ps2_ports[2];

uint8_t ps2_read_irqbuf(uint8_t device) {
	uint8_t ret = ps2_ports[device].buf[0];
	for(uint8_t i = 1; i < 16; i++) ps2_ports[device].buf[i - 1] = ps2_ports[device].buf[i];
	ps2_ports[device].bufptr--;
	return ret;
}

uint8_t ps2_read_status(void) {
	return inb(0x64);
}

uint8_t ps2_read_data(void) {
	while(!(ps2_read_status() & (1 << 0)) && !ps2_ports[0].bufptr && !ps2_ports[1].bufptr);
	uint8_t ret;
	if(ps2_ports[0].bufptr) ret = ps2_read_irqbuf(0);
	else if(ps2_ports[1].bufptr) ret = ps2_read_irqbuf(1);
	else ret = inb(0x60);
	return ret;
}

uint8_t ps2_read_data_device(uint8_t device) {
	while(!(ps2_read_status() & (1 << 0)) && !ps2_ports[device].bufptr);
	uint8_t ret;
	if(ps2_ports[device].bufptr) ret = ps2_read_irqbuf(device);
	else ret = inb(0x60);
	return ret;
}

void ps2_write_data(uint8_t data) {
	while(ps2_read_status() & (1 << 1));
	outb(0x60, data);
}

void ps2_write_command(uint8_t cmd) {
	while(ps2_read_status() & (1 << 1));
	outb(0x64, cmd);
}

void ps2_disable_port(uint8_t port) {
	ps2_write_command((port) ? 0xA7 : 0xAD);
}

void ps2_enable_port(uint8_t port) {
	ps2_write_command((port) ? 0xA8 : 0xAE);
}

uint8_t ps2_get_ccb(void) {
	ps2_write_command(0x20);
	return ps2_read_data();
}

void ps2_set_ccb(uint8_t ccb) {
	ps2_write_command(0x60);
	ps2_write_data(ccb);
}

uint8_t ps2_interface_test(uint8_t port) {
	ps2_write_command((port) ? 0xA9 : 0xAB);
	return ps2_read_data();
}

void ps2_device_send_data(uint8_t device, uint8_t data) {
	if(!ps2_ports[device].exist || !ps2_ports[device].connected) return;
	if(device) ps2_write_command(0xD4);
	ps2_write_data(data);
}

void ps2_reboot(void) {
	ps2_write_command(0xFE);
}

void ps2_device_set_scanning(uint8_t device, uint8_t state) {
	// ps2_ports[device].process = 0;
	for(uint8_t n = 0; n < 10; n++) {
		ps2_device_send_data(device, (state) ? 0xF4 : 0xF5);
		if(ps2_read_data_device(device) == 0xFA) break;
	}
	// ps2_ports[device].process = 1;
}

int8_t ps2_device_reset(uint8_t device) {
	if(!ps2_ports[device].exist || !ps2_ports[device].connected) return -1;
	// ps2_ports[device].process = 0;
	for(uint8_t n = 0; n < 10; n++) {
		ps2_device_send_data(device, 0xFF);
		if(ps2_read_data_device(device) != 0xFA) continue;
		if(ps2_read_data_device(device) != 0xAA) return -2;
		ps2_ports[device].id = 0xFFFF;
		for(uint32_t i = 0; i < 100000; i++) {
			if(ps2_read_status() & (1 << 0) || ps2_ports[device].bufptr) {
				ps2_ports[device].id = (ps2_ports[device].bufptr) ? ps2_read_irqbuf(device) : inb(0x60);
				for(uint32_t j = 0; j < 100000; j++) {
					if(ps2_read_status() & (1 << 0) || ps2_ports[device].bufptr) {
						ps2_ports[device].id <<= 8;
						ps2_ports[device].id |= (ps2_ports[device].bufptr) ? ps2_read_irqbuf(device) : inb(0x60);
						ps2_device_set_scanning(device, 1); // in case it gets turned off
						// ps2_ports[device].process = 1;
						return 0;
					}
				}
				ps2_device_set_scanning(device, 1); // in case it gets turned off
				// ps2_ports[device].process = 1;
				return 0;
			}
		}
		ps2_device_set_scanning(device, 1); // in case it gets turned off
		// ps2_ports[device].process = 1;
		return 0;
	}
	// ps2_ports[device].process = 1;
	return -3;
}

uint8_t ps2_device_get_scset(uint8_t device) {
	if(!ps2_ports[device].exist || !ps2_ports[device].connected || (ps2_ports[device].id != 0xFFFF && (ps2_ports[device].id & 0xFF00) != 0xAB00)) return 0;
	ps2_device_set_scanning(device, 0);
	// ps2_ports[device].process = 0;
	for(uint8_t n = 0; n < 10; n++) {
		ps2_device_send_data(device, 0xF0);
		if(ps2_read_data_device(device) != 0xFA) continue;
		for(uint8_t n2 = 0; n2 < 10; n2++) {
			ps2_device_send_data(device, 0);
			uint8_t ret = ps2_read_data_device(device);
			/* some weird emulators, notably Qemu for Windows build 20160903, does NOT send ACK and send the scancode set number instead */
			if(ret != 0xFA && ret != 1 && ret != 2 && ret != 3 && ret != 0x43 && ret != 0x41 && ret != 0x3F) continue;
			if(ret == 0xFA) {
				uint64_t tbegin = timer_ticks;
				while(timer_ticks - tbegin < 1000000 && ret == 0xFA) {
					/* workaround for Virtual PC 2007 */
					if((ps2_read_status() & (1 << 0)) || ps2_ports[device].bufptr) {
						if(ps2_ports[device].bufptr) ret = ps2_read_irqbuf(device);
						else ret = inb(0x60);
					}
				}
				if(ret == 0xFA) ret = 2;
			}
			ps2_device_set_scanning(device, 1);
			// ps2_ports[device].process = 1;
			/* de-translate */
#ifdef DEBUG
			printf("Device on port %u returned 0x%x on get scancode set command\n", device, ret);
#endif
			switch(ret) {
				case 0x43: ret = 1; break;
				case 0x41: ret = 2; break;
				case 0x3F: ret = 3; break;
				case 1: break;
				case 2: break;
				case 3: break;
				default: ret = 0; break; // invalid
			}
			return ret;
		}
		ps2_device_set_scanning(device, 1);
		// ps2_ports[device].process = 1;
		return 0;
	}
	ps2_device_set_scanning(device, 1);
	// ps2_ports[device].process = 1;
	return 0;
}

uint16_t ps2_device_identify(uint8_t device) {
	if(!ps2_ports[device].exist || !ps2_ports[device].connected) return 0;
	ps2_device_set_scanning(device, 0);
	// ps2_ports[device].process = 0;
	for(uint8_t n = 0; n < 10; n++) {
		ps2_device_send_data(device, 0xF2);
		if(ps2_read_data_device(device) != 0xFA) continue;
		uint16_t ret = 0xFFFF;
		for(uint32_t i = 0; i < 100000; i++) {
			if(ps2_read_status() & (1 << 0) || ps2_ports[device].bufptr) {
				ret = (ps2_ports[device].bufptr) ? ps2_read_irqbuf(device) : inb(0x60);
				for(uint32_t j = 0; j < 100000; j++) {
					if(ps2_read_status() & (1 << 0) || ps2_ports[device].bufptr) {
						ret <<= 8;
						ret |= (ps2_ports[device].bufptr) ? ps2_read_irqbuf(device) : inb(0x60);
						ps2_device_set_scanning(device, 1);
						// ps2_ports[device].process = 1;
						return ret;
					}
				}
				ps2_device_set_scanning(device, 1);
				// ps2_ports[device].process = 1;
				return ret;
			}
		}
		ps2_device_set_scanning(device, 1);
		// ps2_ports[device].process = 1;
		return ret;
	}
	// ps2_ports[device].process = 1;
	return 0xFFFE;
}

uint8_t ps2_scset1_norm[] = {
	KEYCODE(31, 7), KEYCODE(0 , 0), KEYCODE(1 , 1), KEYCODE(2 , 1),
	KEYCODE(3 , 1), KEYCODE(4 , 1), KEYCODE(5 , 1), KEYCODE(6 , 1),
	KEYCODE(7 , 1), KEYCODE(8 , 1), KEYCODE(9 , 1), KEYCODE(10, 1),
	KEYCODE(11, 1), KEYCODE(12, 1), KEYCODE(12, 4), KEYCODE(0 , 2),
	KEYCODE(1 , 2), KEYCODE(2 , 2), KEYCODE(3 , 2), KEYCODE(4 , 2),
	KEYCODE(5 , 2), KEYCODE(6 , 2), KEYCODE(7 , 2), KEYCODE(8 , 2),
	KEYCODE(9 , 2), KEYCODE(10, 2), KEYCODE(11, 2), KEYCODE(12, 2),
	KEYCODE(12, 5), KEYCODE(0 , 5), KEYCODE(1 , 3), KEYCODE(2 , 3),
	KEYCODE(3 , 3), KEYCODE(4 , 3), KEYCODE(5 , 3), KEYCODE(6 , 3),
	KEYCODE(7 , 3), KEYCODE(8 , 3), KEYCODE(9 , 3), KEYCODE(10, 3),
	KEYCODE(11, 3), KEYCODE(0 , 1), KEYCODE(0 , 4), KEYCODE(12, 3),
	KEYCODE(1 , 4), KEYCODE(2 , 4), KEYCODE(3 , 4), KEYCODE(4 , 4),
	KEYCODE(5 , 4), KEYCODE(6 , 4), KEYCODE(7 , 4), KEYCODE(8 , 4),
	KEYCODE(9 , 4), KEYCODE(10, 4), KEYCODE(11, 4), KEYCODE(18, 1),
	KEYCODE(2 , 5), KEYCODE(3 , 5), KEYCODE(0 , 3), KEYCODE(1 , 0),
	KEYCODE(2 , 0), KEYCODE(3 , 0), KEYCODE(4 , 0), KEYCODE(5 , 0),
	KEYCODE(6 , 0), KEYCODE(7 , 0), KEYCODE(8 , 0), KEYCODE(9 , 0),
	KEYCODE(10, 0), KEYCODE(16, 1), KEYCODE(14, 0), KEYCODE(16, 2),
	KEYCODE(17, 2), KEYCODE(18, 2), KEYCODE(19, 1), KEYCODE(16, 3),
	KEYCODE(17, 3), KEYCODE(18, 3), KEYCODE(19, 2), KEYCODE(16, 4),
	KEYCODE(17, 4), KEYCODE(18, 4), KEYCODE(16, 5), KEYCODE(19, 4),
	KEYCODE(31, 7), KEYCODE(31, 7), KEYCODE(31, 7), KEYCODE(11, 0),
	KEYCODE(12, 0)
};

uint8_t ps2_scset1_alt[] = {
	KEYCODE(31, 7), KEYCODE(31, 7), KEYCODE(31, 7), KEYCODE(31, 7),
	KEYCODE(31, 7), KEYCODE(31, 7), KEYCODE(31, 7), KEYCODE(31, 7),
	KEYCODE(31, 7), KEYCODE(31, 7), KEYCODE(31, 7), KEYCODE(31, 7),
	KEYCODE(31, 7), KEYCODE(31, 7), KEYCODE(31, 7), KEYCODE(31, 7),
	KEYCODE(13, 3), KEYCODE(31, 7), KEYCODE(31, 7), KEYCODE(31, 7),
	KEYCODE(31, 7), KEYCODE(31, 7), KEYCODE(31, 7), KEYCODE(31, 7),
	KEYCODE(31, 7), KEYCODE(14, 3), KEYCODE(31, 7), KEYCODE(31, 7),
	KEYCODE(19, 3), KEYCODE(7 , 5), KEYCODE(31, 7), KEYCODE(31, 7),
	KEYCODE(15, 5), KEYCODE(19, 0), KEYCODE(13, 4), KEYCODE(31, 7),
	KEYCODE(14, 3), KEYCODE(31, 7), KEYCODE(31, 7), KEYCODE(31, 7),
	KEYCODE(31, 7), KEYCODE(31, 7), KEYCODE(31, 7), KEYCODE(31, 7),
	KEYCODE(31, 7), KEYCODE(31, 7), KEYCODE(15, 4), KEYCODE(31, 7),
	KEYCODE(15, 3), KEYCODE(31, 7), KEYCODE(14, 6), KEYCODE(31, 7),
	KEYCODE(31, 7), KEYCODE(17, 1), KEYCODE(31, 7), KEYCODE(31, 7),
	KEYCODE(4 , 5), KEYCODE(31, 7), KEYCODE(31, 7), KEYCODE(31, 7),
	KEYCODE(31, 7), KEYCODE(31, 7), KEYCODE(31, 7), KEYCODE(31, 7),
	KEYCODE(31, 7), KEYCODE(31, 7), KEYCODE(31, 7), KEYCODE(31, 7),
	KEYCODE(31, 7), KEYCODE(31, 7), KEYCODE(31, 7), KEYCODE(14, 1),
	KEYCODE(8 , 5), KEYCODE(15, 1), KEYCODE(31, 7), KEYCODE(10, 5),
	KEYCODE(31, 7), KEYCODE(11, 5), KEYCODE(31, 7), KEYCODE(14, 2),
	KEYCODE(9 , 5), KEYCODE(15, 2), KEYCODE(13, 1), KEYCODE(13, 2),
	KEYCODE(31, 7), KEYCODE(31, 7), KEYCODE(31, 7), KEYCODE(31, 7),
	KEYCODE(31, 7), KEYCODE(31, 7), KEYCODE(31, 7), KEYCODE(1 , 5),
	KEYCODE(5 , 5), KEYCODE(6 , 5), KEYCODE(16, 6), KEYCODE(17, 6),
	KEYCODE(31, 7), KEYCODE(31, 7), KEYCODE(31, 7), KEYCODE(18, 6),
	KEYCODE(31, 7), KEYCODE(19, 5), KEYCODE(16, 0), KEYCODE(17, 5),
	KEYCODE(18, 5), KEYCODE(14, 5), KEYCODE(13, 5), KEYCODE(17, 0),
	KEYCODE(18, 0), KEYCODE(13, 6)
};

uint8_t ps2_scset2_norm[] = {
	KEYCODE(31, 7), KEYCODE(9 , 0), KEYCODE(31, 7), KEYCODE(5 , 0),
	KEYCODE(3 , 0), KEYCODE(1 , 0), KEYCODE(2 , 0), KEYCODE(12, 0),
	KEYCODE(31, 7), KEYCODE(10, 0), KEYCODE(8 , 0), KEYCODE(6 , 0),
	KEYCODE(4 , 0), KEYCODE(0 , 2), KEYCODE(0 , 1), KEYCODE(31, 7),
	KEYCODE(31, 7), KEYCODE(2 , 5), KEYCODE(0 , 4), KEYCODE(31, 7),
	KEYCODE(0 , 5), KEYCODE(1 , 2), KEYCODE(1 , 1), KEYCODE(31, 7),
	KEYCODE(31, 7), KEYCODE(31, 7), KEYCODE(1 , 4), KEYCODE(2 , 3),
	KEYCODE(1 , 3), KEYCODE(2 , 2), KEYCODE(2 , 1), KEYCODE(31, 7),
	KEYCODE(31, 7), KEYCODE(3 , 4), KEYCODE(2 , 4), KEYCODE(3 , 3),
	KEYCODE(3 , 2), KEYCODE(4 , 1), KEYCODE(3 , 1), KEYCODE(31, 7),
	KEYCODE(31, 7), KEYCODE(3 , 5), KEYCODE(4 , 4), KEYCODE(4 , 3),
	KEYCODE(5 , 2), KEYCODE(4 , 2), KEYCODE(5 , 1), KEYCODE(31, 7),
	KEYCODE(31, 7), KEYCODE(6 , 4), KEYCODE(5 , 4), KEYCODE(6 , 3),
	KEYCODE(5 , 3), KEYCODE(6 , 2), KEYCODE(6 , 1), KEYCODE(31, 7),
	KEYCODE(31, 7), KEYCODE(31, 7), KEYCODE(7 , 4), KEYCODE(7 , 3),
	KEYCODE(7 , 2), KEYCODE(7 , 1), KEYCODE(8 , 1), KEYCODE(31, 7),
	KEYCODE(31, 7), KEYCODE(8 , 4), KEYCODE(8 , 3), KEYCODE(8 , 2),
	KEYCODE(9 , 2), KEYCODE(10, 1), KEYCODE(9 , 1), KEYCODE(31, 7),
	KEYCODE(31, 7), KEYCODE(9 , 4), KEYCODE(10, 4), KEYCODE(9 , 3),
	KEYCODE(10, 3), KEYCODE(10, 2), KEYCODE(11, 1), KEYCODE(31, 7),
	KEYCODE(31, 7), KEYCODE(31, 7), KEYCODE(11, 3), KEYCODE(31, 7),
	KEYCODE(11, 2), KEYCODE(12, 1), KEYCODE(31, 7), KEYCODE(31, 7),
	KEYCODE(0 , 3), KEYCODE(11, 4), KEYCODE(12, 5), KEYCODE(12, 2),
	KEYCODE(31, 7), KEYCODE(12, 3), KEYCODE(31, 7), KEYCODE(31, 7),
	KEYCODE(31, 7), KEYCODE(31, 7), KEYCODE(31, 7), KEYCODE(31, 7),
	KEYCODE(31, 7), KEYCODE(31, 7), KEYCODE(12, 4), KEYCODE(31, 7),
	KEYCODE(31, 7), KEYCODE(16, 4), KEYCODE(31, 7), KEYCODE(16, 3),
	KEYCODE(16, 2), KEYCODE(31, 7), KEYCODE(31, 7), KEYCODE(31, 7),
	KEYCODE(16, 5), KEYCODE(19, 4), KEYCODE(17, 4), KEYCODE(17, 3),
	KEYCODE(18, 3), KEYCODE(17, 2), KEYCODE(0 , 0), KEYCODE(16, 1),
	KEYCODE(11, 0), KEYCODE(19, 2), KEYCODE(18, 4), KEYCODE(19, 1),
	KEYCODE(18, 1), KEYCODE(18, 2), KEYCODE(14, 0), KEYCODE(31, 7),
	KEYCODE(31, 7), KEYCODE(31, 7), KEYCODE(31, 7), KEYCODE(7 , 0)
};

uint8_t ps2_scset2_alt[] = {
	KEYCODE(31, 7), KEYCODE(31, 7), KEYCODE(31, 7), KEYCODE(31, 7),
	KEYCODE(31, 7), KEYCODE(31, 7), KEYCODE(31, 7), KEYCODE(31, 7),
	KEYCODE(31, 7), KEYCODE(31, 7), KEYCODE(31, 7), KEYCODE(31, 7),
	KEYCODE(31, 7), KEYCODE(31, 7), KEYCODE(31, 7), KEYCODE(31, 7),
	KEYCODE(19, 5), KEYCODE(4 , 5), KEYCODE(31, 7), KEYCODE(31, 7),
	KEYCODE(7 , 5), KEYCODE(13, 3), KEYCODE(31, 7), KEYCODE(31, 7),
	KEYCODE(16, 0), KEYCODE(31, 7), KEYCODE(31, 7), KEYCODE(31, 7),
	KEYCODE(31, 7), KEYCODE(31, 7), KEYCODE(31, 7), KEYCODE(1 , 5),
	KEYCODE(17, 5), KEYCODE(15, 4), KEYCODE(31, 7), KEYCODE(15, 5),
	KEYCODE(31, 7), KEYCODE(31, 7), KEYCODE(31, 7), KEYCODE(5 , 5),
	KEYCODE(18, 5), KEYCODE(31, 7), KEYCODE(31, 7), KEYCODE(19, 0),
	KEYCODE(31, 7), KEYCODE(31, 7), KEYCODE(31, 7), KEYCODE(6 , 5),
	KEYCODE(14, 5), KEYCODE(31, 7), KEYCODE(15, 3), KEYCODE(31, 7),
	KEYCODE(13, 4), KEYCODE(31, 7), KEYCODE(31, 7), KEYCODE(16, 6),
	KEYCODE(13, 5), KEYCODE(31, 7), KEYCODE(14, 6), KEYCODE(14, 4),
	KEYCODE(31, 7), KEYCODE(31, 7), KEYCODE(31, 7), KEYCODE(17, 6),
	KEYCODE(17, 0), KEYCODE(31, 7), KEYCODE(31, 7), KEYCODE(31, 7),
	KEYCODE(31, 7), KEYCODE(31, 7), KEYCODE(31, 7), KEYCODE(31, 7),
	KEYCODE(18, 0), KEYCODE(31, 7), KEYCODE(17, 1), KEYCODE(31, 7),
	KEYCODE(31, 7), KEYCODE(14, 3), KEYCODE(31, 7), KEYCODE(31, 7),
	KEYCODE(13, 6), KEYCODE(31, 7), KEYCODE(31, 7), KEYCODE(31, 7),
	KEYCODE(31, 7), KEYCODE(31, 7), KEYCODE(31, 7), KEYCODE(31, 7),
	KEYCODE(31, 7), KEYCODE(31, 7), KEYCODE(19, 3), KEYCODE(31, 7),
	KEYCODE(31, 7), KEYCODE(31, 7), KEYCODE(18, 6), KEYCODE(31, 7),
	KEYCODE(31, 7), KEYCODE(31, 7), KEYCODE(31, 7), KEYCODE(31, 7),
	KEYCODE(31, 7), KEYCODE(31, 7), KEYCODE(31, 7), KEYCODE(31, 7),
	KEYCODE(31, 7), KEYCODE(14, 2), KEYCODE(31, 7), KEYCODE(10, 5),
	KEYCODE(14, 1), KEYCODE(31, 7), KEYCODE(31, 7), KEYCODE(31, 7),
	KEYCODE(13, 1), KEYCODE(13, 2), KEYCODE(9 , 5), KEYCODE(31, 7),
	KEYCODE(11, 5), KEYCODE(8 , 5), KEYCODE(31, 7), KEYCODE(31, 7),
	KEYCODE(31, 7), KEYCODE(31, 7), KEYCODE(15, 2), KEYCODE(31, 7),
	KEYCODE(31, 7), KEYCODE(15, 1)
};

void ps2_process(uint8_t device, uint8_t code) {
	if(ps2_ports[device].id == 0xFFFF || (ps2_ports[device].id & 0xFF00) == 0xAB00) {
		/* keyboard */
		if(code == 0xE0) {
			ps2_ports[device].altlut = 1;
			return;
		} else if(ps2_ports[device].scset == 1) {
			/* set 1 */
			if(ps2_ports[device].special == 2 && ps2_ports[device].skip_rem) {
				ps2_ports[device].skip_rem--;
				return;
			}
			if(code & 0x80) ps2_ports[device].brk = 1;
			else ps2_ports[device].brk = 0;
			if(ps2_ports[device].altlut && code == 0x2A) {
				ps2_ports[device].special = 1;
			} else if(ps2_ports[device].altlut && code == 0x37 && ps2_ports[device].special == 1) {
				ps2_ports[device].special = 0;
				kb_register(KEYCODE(13, 0), 1);
			} else if(ps2_ports[device].altlut && code == 0xB7) {
				ps2_ports[device].special = 1;
			} else if(ps2_ports[device].altlut && code == 0xAA && ps2_ports[device].special == 1) {
				ps2_ports[device].special = 0;
				kb_register(KEYCODE(13, 0), 0);
			} else if(code == 0xE1 && ps2_ports[device].special != 2) {
				ps2_ports[device].special = 2;
				ps2_ports[device].skip_rem = 2;
				kb_register(KEYCODE(15, 0), 1);
			} else if(code == 0xE1 && ps2_ports[device].special == 2) {
				ps2_ports[device].special = 0;
				ps2_ports[device].skip_rem = 2;
				kb_register(KEYCODE(15, 0), 0);
			} else if(ps2_ports[device].altlut) kb_register(ps2_scset1_alt[(code & 0x7F)], 1 - ps2_ports[device].brk);
			else kb_register(ps2_scset1_norm[(code & 0x7F)], 1 - ps2_ports[device].brk);
			ps2_ports[device].brk = 0; ps2_ports[device].altlut = 0;
		} else if(ps2_ports[device].scset == 2 || ps2_ports[device].scset == 0) { /* assume scancode set 0 is 2, as that's what some BIOSes do */
			/* set 2 */
			if(ps2_ports[device].special == 2 && ps2_ports[device].skip_rem) {
				ps2_ports[device].skip_rem--;
				return;
			}
			if(code == 0xF0) {
				ps2_ports[device].brk = 1;
				return;
			}
			if(ps2_ports[device].altlut && !ps2_ports[device].brk && code == 0x12) {
				ps2_ports[device].special = 1;
			} else if(ps2_ports[device].altlut && !ps2_ports[device].brk && code == 0x7C && ps2_ports[device].special == 1) {
				ps2_ports[device].special = 0;
				kb_register(KEYCODE(13, 0), 1);
			} else if(ps2_ports[device].altlut && ps2_ports[device].brk && code == 0x7C) {
				ps2_ports[device].special = 1;
			} else if(ps2_ports[device].altlut && ps2_ports[device].brk && code == 0x12) {
				ps2_ports[device].special = 0;
				kb_register(KEYCODE(13, 0), 0);
				ps2_ports[device].brk = 0;
			} else if(code == 0xE1 && ps2_ports[device].special != 2) {
				ps2_ports[device].special = 2;
				ps2_ports[device].skip_rem = 2;
				kb_register(KEYCODE(15, 0), 1);
			} else if(code == 0xE1 && ps2_ports[device].special == 2) {
				ps2_ports[device].special = 0;
				ps2_ports[device].skip_rem = 4;
				kb_register(KEYCODE(15, 0), 0);
			} else if(ps2_ports[device].altlut) kb_register(ps2_scset2_alt[code], 1 - ps2_ports[device].brk);
			else kb_register(ps2_scset2_norm[code], 1 - ps2_ports[device].brk);
			ps2_ports[device].brk = 0; ps2_ports[device].altlut = 0;
		}
	} else if(ps2_ports[device].id == 0x00) {
		/* mouse */
		ps2_ports[device].mousebuf[ps2_ports[device].mousebuf_ptr++] = code;
		if(ps2_ports[device].mousebuf_ptr == 3) {
			ps2_ports[device].mousebuf_ptr = 0;
			mouse_buttons = ps2_ports[device].mousebuf[0] & 0x07;
			int32_t dx = ps2_ports[device].mousebuf[1] - ((ps2_ports[device].mousebuf[0] << 4) & 0x100);
			int32_t dy = ps2_ports[device].mousebuf[2] - ((ps2_ports[device].mousebuf[0] << 3) & 0x100);
			mouse_update(dx, -dy);
		}
	}
}

void ps2_handler0(void) {
	uint8_t t = inb(0x60); // printf("[0: 0x%x]", t);
	if(ps2_ports[0].process) {
		ps2_process(0, t);
	} else {
		ps2_ports[0].buf[ps2_ports[0].bufptr++] = t;
		if(ps2_ports[0].bufptr == 16) ps2_read_irqbuf(0);
		// printf("Received 0x%x from device 0\n", ps2_ports[0].buf[ps2_ports[0].bufptr - 1]);
	}
	pic_eoi(1);
	if((ps2_ports[0].id == 0xFFFF || (ps2_ports[0].id & 0xFF00) == 0xAB00) && ps2_ports[0].process) {
		asm("sti");
		kb_postreg();
	}
}

void ps2_handler1(void) {
	uint8_t t = inb(0x60); // printf("[1: 0x%x]", t);
	if(ps2_ports[1].process) {
		ps2_process(1, t);
	} else {
		ps2_ports[1].buf[ps2_ports[1].bufptr++] = t;
		if(ps2_ports[1].bufptr == 16) ps2_read_irqbuf(0);
		// printf("Received 0x%x from device 1\n", ps2_ports[1].buf[ps2_ports[1].bufptr - 1]);
	}
	pic_eoi(12);
	if((ps2_ports[1].id == 0xFFFF || (ps2_ports[1].id & 0xFF00) == 0xAB00) && ps2_ports[1].process) {
		asm("sti");
		kb_postreg();
	}
}

int8_t ps2_init(void) {
	/* assume all ports are operational */
#ifdef DEBUG
	printf("Assuming all ports are operational\n");
#endif
	ps2_ports[0].exist = 1; ps2_ports[0].connected = 1; ps2_ports[0].bufptr = 0; ps2_ports[0].process = 0;
	ps2_ports[1].exist = 1; ps2_ports[1].connected = 1; ps2_ports[1].bufptr = 0; ps2_ports[1].process = 0;
	/* disable ports */
#ifdef DEBUG
	printf("Disabling PS/2 ports\n");
#endif
	ps2_disable_port(0); ps2_disable_port(1);
	/* flush the output buffer */
#ifdef DEBUG
	printf("Flushing the output buffer\n");
#endif
	for(uint8_t i = 0; i < 200; i++) inb(0x60);
	/* set the CCB */
#ifdef DEBUG
	printf("Setting up the initial CCB\n");
#endif
	ps2_set_ccb(ps2_get_ccb() & ~((1 << 0) | (1 << 1) | (1 << 6))); // proper CCB config
	if(!(ps2_get_ccb() & (1 << 5))) ps2_ports[1].exist = 0;
	/* perform controller self test */
#ifdef DEBUG
	printf("Performing controller self test\n");
#endif
	ps2_write_command(0xAA);
	if(ps2_read_data() != 0x55) return -1; // self test failed
	ps2_set_ccb(ps2_get_ccb() & ~((1 << 0) | (1 << 1) | (1 << 6)));
	/* determine if there are 2 channels */
	if(ps2_ports[1].exist) {
		ps2_enable_port(1);
		if(ps2_get_ccb() & (1 << 5)) {
			ps2_ports[1].exist = 0;
		} else ps2_disable_port(1);
	}
#ifdef DEBUG
	printf("Second PS/2 port exists = %u\n", ps2_ports[1].exist);
#endif
	/* perform interface tests */
	for(uint8_t i = 0; i < 2; i++) {
#ifdef DEBUG
		printf("Performing interface test on port %u\n", i);
#endif
		if(ps2_ports[i].exist && ps2_interface_test(i) != 0) ps2_ports[i].exist = 0;
#ifdef DEBUG
		printf("PS/2 port %u working = %u\n", i, ps2_ports[i].exist);
#endif
	}
	if(ps2_ports[0].exist == 0 && ps2_ports[1].exist == 0) return -2; // no working ports
	/* enable devices */
	for(uint8_t i = 0; i < 2; i++) {
		if(ps2_ports[i].exist) {
#ifdef DEBUG
			printf("Enabling port %u\n", i);
#endif
			ps2_enable_port(i);
		}
	}
#ifdef DEBUG
	printf("Re-setting CCB\n");
#endif
	ps2_set_ccb(ps2_get_ccb() & ~((1 << 0) | (1 << 1) | (1 << 6))); // set the CCB to what it's supposed to be
#ifdef DEBUG
	printf("CCB = 0x%x\n", ps2_get_ccb());
#endif
	/* reset devices and get their IDs, get keyboards' scancode set and turn on IRQ */
	for(uint8_t i = 0; i < 2; i++) {
		if(ps2_ports[i].exist) {
#ifdef DEBUG
			printf("Resetting device on port %u\n", i);
#endif
			if(ps2_device_reset(i) != 0) ps2_ports[i].connected = 0;
			else ps2_ports[i].id = ps2_device_identify(i);
#ifdef DEBUG
			printf("Enabling IRQ on port %u\n", i);
#endif
			ps2_set_ccb(ps2_get_ccb() | ((i) ? (1 << 1) : (1 << 0)));
			pic_irqhandlers[(i) ? 12 : 1] = (i) ? ps2_handler1 : ps2_handler0;
			pic_mask((i) ? 12 : 1, 0);
#ifdef DEBUG
			printf("Getting scancode set of device on port %u\n", i);
#endif
			ps2_ports[i].scset = ps2_device_get_scset(i);
			//ps2_ports[i].process = ps2_ports[i].connected;
			ps2_ports[i].altlut = 0; ps2_ports[i].brk = 0;
			ps2_ports[i].mousebuf_ptr = 0;
		}
#ifdef DEBUG
		printf("Port %u: exist = %u, connected = %u, id = 0x%x, scset = 0x%x\n", i, ps2_ports[i].exist, ps2_ports[i].connected, ps2_ports[i].id, ps2_ports[i].scset);
#endif
		if(!i) usleep(100000); // 100ms cooldown timer
		ps2_ports[i].bufptr = 0;
	}
	for(uint8_t i = 0; i < 2; i++) {
		ps2_ports[i].process = ps2_ports[i].connected;
		pic_eoi((i) ? 12 : 1);
		ps2_ports[i].mousebuf_ptr = 0;
		ps2_ports[i].bufptr = 0;
		ps2_ports[i].altlut = 0;
		ps2_ports[i].brk = 0;
		ps2_ports[i].special = 0;
		ps2_ports[i].skip_rem = 0;
	}
#ifdef DEBUG
	printf("PS/2 initialization completed successfully\n");
#endif
	return 0;
}
