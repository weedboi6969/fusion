; Copyright (C) 2020 Succboy6000 (Thanh Vinh Nguyen).

; This program is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.

; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.

; You should have received a copy of the GNU General Public License
; along with this program.  If not, see <https://www.gnu.org/licenses/>.

%ifndef COMMON_STRING
global memcpy
global memset
global memset_16
global memset_32

memcpy:
push ebp
mov ebp, esp
push esi
push edi
push ecx
push edx
mov edi, [ebp + 8]
push edi
mov esi, [ebp + 12]
mov edx, [ebp + 16]
mov ecx, edx
shr ecx, 2
cld
rep movsd
mov ecx, edx
and ecx, 3
rep movsb
pop eax
pop edx
pop ecx
pop edi
pop esi
mov esp, ebp
pop ebp
ret

memset:
push ebp
mov ebp, esp
push ebx
push edi
push ecx
push edx
mov edi, [ebp + 8]
push edi
xor ebx, ebx
mov bl, [ebp + 12]
mov bh, [ebp + 12]
mov edx, [ebp + 16]
mov eax, ebx
shl ebx, 16
or eax, ebx
mov ecx, edx
shr ecx, 2
cld
rep stosd
mov ecx, edx
and ecx, 3
rep stosb
pop eax
pop edx
pop ecx
pop edi
pop ebx
mov esp, ebp
pop ebp
ret

memset_16:
push ebp
mov ebp, esp
push ebx
push edi
push ecx
push edx
mov edi, [ebp + 8]
push edi
xor ebx, ebx
mov bx, [ebp + 12]
mov edx, [ebp + 16]
mov eax, ebx
shl ebx, 16
or eax, ebx
mov ecx, edx
shr ecx, 1
cld
rep stosd
mov ecx, edx
and ecx, 1
rep stosw
pop eax
pop edx
pop ecx
pop edi
pop ebx
mov esp, ebp
pop ebp
ret

memset_32:
push ebp
mov ebp, esp
push edi
push ecx
push edx
mov edi, [ebp + 8]
push edi
mov eax, [ebp + 12]
mov edx, [ebp + 16]
mov ecx, edx
cld
rep stosd
pop eax
pop edx
pop ecx
pop edi
mov esp, ebp
pop ebp
ret

%endif
