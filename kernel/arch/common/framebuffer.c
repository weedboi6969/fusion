/*
 * Copyright (C) 2020 Succboy6000 (Thanh Vinh Nguyen).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <framebuffer.h>
#include <stdlib.h>
#include <string.h>
#include <devfs.h>

fb_device_t *fb_default = NULL;

void fb_putpixel(fb_device_t *dev, uint32_t x, uint32_t y, uint32_t color) {
	if(dev == NULL || x >= dev->width || y >= dev->height) return;
	uint32_t r = (color >> 16) & 0xFF; if(dev->color.r_bits >= 8) r <<= dev->color.r_bits - 8; else r >>= 8 - dev->color.r_bits;
	uint32_t g = (color >> 8) & 0xFF; if(dev->color.g_bits >= 8) g <<= dev->color.g_bits - 8; else g >>= 8 - dev->color.g_bits;
	uint32_t b = color & 0xFF; if(dev->color.b_bits >= 8) b <<= dev->color.b_bits - 8; else b >>= 8 - dev->color.b_bits;
	uint32_t out = (r << dev->color.r_start) | (g << dev->color.g_start) | (b << dev->color.b_start);
	/* TODO: optimize */
	uint8_t *pix = &dev->buffer[y * dev->pitch + x * dev->color.bytes];
	for(uint32_t i = 0; i < dev->color.bytes; i++) {
		if(dev->color.endian) {
			/* big endian */
			pix[i] = (out >> ((dev->color.bytes - i - 1) * 8)) & 0xFF;
		} else {
			/* small endian */
			pix[i] = (out >> (i * 8)) & 0xFF;
		}
	}
}

uint32_t fb_getpixel(fb_device_t *dev, uint32_t x, uint32_t y) {
	if(dev == NULL || x >= dev->width || y >= dev->height) return 0;
	/* TODO: optimize */
	uint8_t *pix = &dev->buffer[y * dev->pitch + x * ((dev->bpp + 7) / 8)];
	uint32_t color = 0;
	for(uint32_t i = 0; i < dev->color.bytes; i++) {
		if(dev->color.endian) {
			/* big endian */
			color |= pix[i] << ((dev->color.bytes - i - 1) * 8);
		} else {
			/* small endian */
			color |= pix[i] << (i * 8);
		}
	}
	uint32_t r = color >> dev->color.r_start; if(dev->color.r_bits >= 8) r >>= dev->color.r_bits - 8; else r <<= 8 - dev->color.r_bits; r &= 0xFF;
	uint32_t g = color >> dev->color.g_start; if(dev->color.g_bits >= 8) g >>= dev->color.g_bits - 8; else g <<= 8 - dev->color.g_bits; g &= 0xFF;
	uint32_t b = color >> dev->color.b_start; if(dev->color.b_bits >= 8) b >>= dev->color.b_bits - 8; else b <<= 8 - dev->color.b_bits; b &= 0xFF;
	return rgb(r, g, b);
}

void fb_fill(fb_device_t *dev, uint32_t color) {
	if(dev == NULL) return;
	if(dev->color.bytes == 1 || dev->color.bytes == 2 || dev->color.bytes == 4) {
		uint32_t r = (color >> 16) & 0xFF; if(dev->color.r_bits >= 8) r <<= dev->color.r_bits - 8; else r >>= 8 - dev->color.r_bits;
		uint32_t g = (color >> 8) & 0xFF; if(dev->color.g_bits >= 8) g <<= dev->color.g_bits - 8; else g >>= 8 - dev->color.g_bits;
		uint32_t b = color & 0xFF; if(dev->color.b_bits >= 8) b <<= dev->color.b_bits - 8; else b >>= 8 - dev->color.b_bits;
		uint32_t out = (r << dev->color.r_start) | (g << dev->color.g_start) | (b << dev->color.b_start);
		switch(dev->color.bytes) {
			case 1: memset((void*) dev->buffer, out, dev->width * dev->height); return;
			case 2: memset_16((void*) dev->buffer, out, dev->width * dev->height); return;
			case 4: memset_32((void*) dev->buffer, out, dev->width * dev->height); return;
		}
	}
	for(uint32_t y = 0; y < dev->height; y++) {
		for(uint32_t x = 0; x < dev->width; x++) fb_putpixel(dev, x, y, color);
	}
}

void fb_scrollup(fb_device_t *dev, uint32_t lines, uint32_t y0, uint32_t y1) {
	if(dev == NULL) return;
	if(!lines || y0 >= y1) return;
	memcpy((void*) &dev->buffer[dev->pitch * y0], (void*) &dev->buffer[dev->pitch * (y0 + lines)], dev->pitch * (y1 - y0 + 1 - lines));
	memset((void*) &dev->buffer[dev->pitch * (y1 + 1 - lines)], 0, dev->pitch * lines);
}

void fb_drawline(fb_device_t *dev, uint32_t x1, uint32_t y1, uint32_t x2, uint32_t y2, uint32_t color) {
	if(dev == NULL) return;
	if(x1 >= dev->width || y1 >= dev->height) return;
	/* Bresenham's algorithm */
	// (int) x1; (int) x2; (int) y1; (int) y2;
	uint8_t steep = abs(y2 - y1) > abs(x2 - x1);
	int t;
	if(steep) {
		t = x1;
		x1 = y1;
		y1 = t;
		t = x2;
		x2 = y2;
		y2 = t;
	}

	if(x1 > x2) {
		t = x1;
		x1 = x2;
		x2 = t;
		t = y1;
		y1 = y2;
		y2 = t;
	}

	int32_t dx, dy;
	dx = x2 - x1;
	dy = abs(y2 - y1);

	int32_t err = dx / 2;
	int32_t ystep;

	if(y1 < y2) ystep = 1;
	else ystep = -1;

	for(; x1 <= x2; x1++) {
		if(steep) fb_putpixel(dev, y1, x1, color);
		else fb_putpixel(dev, x1, y1, color);
		err -= dy;
		if(err < 0) {
			y1 += ystep;
			err += dx;
		}
	}
}

void fb_drawrect(fb_device_t *dev, uint32_t x1, uint32_t y1, uint32_t x2, uint32_t y2, uint32_t color) {
	if(dev == NULL) return;
	if(x1 >= dev->width || y1 >= dev->height) return;
	fb_drawline(dev, x1, y1, x2, y1, color);
	fb_drawline(dev, x2, y1, x2, y2, color);
	fb_drawline(dev, x2, y2, x1, y2, color);
	fb_drawline(dev, x1, y2, x1, y1, color);
}

void fb_fillrect(fb_device_t *dev, uint32_t x1, uint32_t y1, uint32_t x2, uint32_t y2, uint32_t color) {
	if(dev == NULL) return;
	if(x1 >= dev->width || y1 >= dev->height) return;
	uint32_t t;
	if(y1 > y2) {
		t = y1;
		y1 = y2;
		y2 = t;
	}
	for(uint32_t y = y1; y <= y2; y++) fb_drawline(dev, x1, y, x2, y, color);
}

void fb_drawbitmap(fb_device_t *dev, uint32_t x, uint32_t y, uint32_t *bitmap, uint32_t width, uint32_t height) {
	if(dev == NULL) return;
	if(x >= dev->width || y >= dev->height) return;
	for(uint32_t i = 0; i < height; i++) {
		for(uint32_t j = 0; j < width; j++) fb_putpixel(dev, x + j, y + i, bitmap[i * width + j]);
	}
}

/* framebuffer device (/dev/fb0) functions */
vfs_node_t *fbdev = NULL;

uint32_t fb_vfs_read(vfs_node_t *node, uint32_t offset, uint32_t size, uint8_t *buf) {
	if(!devfs_isopen(node)) return 0; // we can't read from closed device
	if(!size) size = fb_default->height * fb_default->width;
	else size /= 4;
	offset /= 4;
	uint32_t *buf32 = (uint32_t*) buf;
	for(uint32_t i = offset; i < (offset + size); i++) {
		buf32[i - offset] = fb_getpixel(fb_default, (i % fb_default->width), (i / fb_default->width));
	}
	return size * 4;
}

uint32_t fb_vfs_write(vfs_node_t *node, uint32_t offset, uint32_t size, uint8_t *buf) {
	(void) offset; // there's no offset for stream devices
	if(!devfs_isopen(node)) return 0; // we can't read from closed device
	if(!size) size = fb_default->height * fb_default->width;
	else size /= 4;
	offset /= 4;
	uint32_t *buf32 = (uint32_t*) buf;
	for(uint32_t i = offset; i < (offset + size); i++) {
		fb_putpixel(fb_default, (i % fb_default->width), (i / fb_default->width), buf32[i - offset]);
	}
	return size * 4;
}

int fb_vfs_init(void) {
	if(fbdev != NULL) return 0;
	fbdev = devfs_register("fb0", (void*) fb_vfs_read, (void*) fb_vfs_write);
	return 0; // success
}
