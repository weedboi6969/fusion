/*
 * Copyright (C) 2020 Succboy6000 (Thanh Vinh Nguyen).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <elf32.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

/* global symbols table */
elf32_gstab_t *elf32_gstab = NULL;
uint32_t elf32_gstab_ents = 0;

static inline elf32_shdr_t *elf32_secthdr(elf32_hdr_t *hdr) {
	return (elf32_shdr_t*) ((uintptr_t) hdr + hdr->e_shoff);
}

static inline elf32_shdr_t *elf32_section(elf32_hdr_t *hdr, uint32_t idx) {
	return (elf32_shdr_t*) ((uintptr_t) hdr + hdr->e_shoff + idx * hdr->e_shentsize);
}

static inline char *elf32_shstrtab(elf32_hdr_t *hdr) {
	if(hdr->e_shstrndx == SHN_UNDEF) return NULL; // no string table
	return (char*) ((uintptr_t) hdr + elf32_section(hdr, hdr->e_shstrndx)->sh_offset);
}

static inline char *elf32_shstring(elf32_hdr_t *hdr, uint32_t offset) {
	char *strtab = elf32_shstrtab(hdr);
	if(strtab == NULL) return NULL;
	return (char*) ((uintptr_t) strtab + offset);
}

static uint32_t elf32_lookup_sym(const char *name) {
	/* look up external symbol */
	for(uint32_t i = 0; i < elf32_gstab_ents; i++) {
		if(!strcmp(elf32_gstab[i].name, name) && elf32_gstab[i].bind != STB_LOCAL) return elf32_gstab[i].ptr;
	}
	return 0;
}

static uint32_t elf32_get_symval(elf32_hdr_t *hdr, uint32_t tab, uint32_t idx) {
	if(tab == SHN_UNDEF || idx == SHN_UNDEF) return 0;
	elf32_shdr_t *symtab = elf32_section(hdr, tab);
	uint32_t symtab_ents = symtab->sh_size / symtab->sh_entsize;
	if(idx >= symtab_ents) {
		printf("Error getting symbol %u:%u value: Index out of range (max %u)\n", tab, idx, symtab_ents - 1);
		return 0xFFFFFFFF;
	}
	elf32_sym_t *sym = (elf32_sym_t*) ((uintptr_t) hdr + symtab->sh_offset + idx * sizeof(elf32_sym_t));
	if(sym->st_shndx == SHN_UNDEF) {
		/* external symbol */
		elf32_shdr_t *strtab = elf32_section(hdr, symtab->sh_link);
		const char *name = (const char*) ((uintptr_t) hdr + strtab->sh_offset + sym->st_name);
		uint32_t target = elf32_lookup_sym(name);
		if(!target) {
			/* external symbol not found */
			if(ELF32_ST_BIND(sym->st_info) & STB_WEAK) return 0;
			else {
				printf("Error getting symbol %u:%u value: Undefined external symbol %s\n", tab, idx, name);
				return 0xFFFFFFFF;
			}
		} else return target;
	} else if(sym->st_shndx == SHN_ABS) return sym->st_value;
	elf32_shdr_t *target = elf32_section(hdr, sym->st_shndx);
	return ((uintptr_t) hdr + sym->st_value + target->sh_offset);
}

static uint32_t elf32_reloc(elf32_hdr_t *hdr, elf32_rel_t *rel, elf32_shdr_t *reltab) {
	elf32_shdr_t *target = elf32_section(hdr, reltab->sh_info);
	uint32_t *ref = (uint32_t*) ((uintptr_t) hdr + target->sh_offset + rel->r_offset);
	uint32_t symval = 0;
	if(ELF32_R_SYM(rel->r_info) != SHN_UNDEF) {
		symval = elf32_get_symval(hdr, reltab->sh_link, ELF32_R_SYM(rel->r_info));
		if(symval == 0xFFFFFFFF) return 0xFFFFFFFF;
	}
	switch(ELF32_R_TYPE(rel->r_info)) {
		case R_386_NONE: break;
		case R_386_32: *ref = DO_386_32(symval, *ref); break;
		case R_386_PC32: *ref = DO_386_PC32(symval, *ref, (uintptr_t) ref); break;
		default: printf("Error relocating reference 0x%x: Unsupported relocation type %u\n", ref, ELF32_R_TYPE(rel->r_info)); return 0xFFFFFFFF;
	}
	return symval;
}

static void elf32_add_sym(char *name, uint16_t type, uint16_t bind, uintptr_t ptr, uintptr_t size) {
	elf32_gstab = realloc(elf32_gstab, ++elf32_gstab_ents * sizeof(elf32_gstab_t));
	strcpy(elf32_gstab[elf32_gstab_ents - 1].name, name);
	elf32_gstab[elf32_gstab_ents - 1].type = type;
	elf32_gstab[elf32_gstab_ents - 1].bind = bind;
	elf32_gstab[elf32_gstab_ents - 1].ptr = ptr;
	elf32_gstab[elf32_gstab_ents - 1].size = size;
}

static uint32_t elf32_find_sym(elf32_hdr_t *hdr, char *name) {
	for(uint32_t i = 1; i < hdr->e_shnum; i++) {
		elf32_shdr_t *shdr = elf32_section(hdr, i);
		if(shdr->sh_type == SHT_SYMTAB) {
			/* we've found a symbol table, dive deeper */
			for(uint32_t j = 1; j < shdr->sh_size / shdr->sh_entsize; j++) {
				elf32_sym_t *sym = (elf32_sym_t*) ((uintptr_t) hdr + shdr->sh_offset + j * sizeof(elf32_sym_t));
				if((ELF32_ST_TYPE(sym->st_info) == STT_FUNC || ELF32_ST_TYPE(sym->st_info) == STT_OBJECT || ELF32_ST_TYPE(sym->st_info) == STT_NOTYPE) && (ELF32_ST_BIND(sym->st_info) == STB_GLOBAL || ELF32_ST_BIND(sym->st_info) == STB_WEAK) && sym->st_shndx != SHN_UNDEF) {
					elf32_shdr_t *strtab = elf32_section(hdr, shdr->sh_link);
					char *sname = (char*) ((uintptr_t) hdr + strtab->sh_offset + sym->st_name);
					if(!strcmp(name, sname)) return elf32_get_symval(hdr, i, j);
				}
			}
		}
	}
	return 0; // cannot find symbol
}

uintptr_t elf32_load(void *file, uint32_t size, uint32_t mode, uintptr_t *new_addr) {
	elf32_hdr_t *hdr = (elf32_hdr_t*) file;
	/* check if we can load this file */
	if(strncmp(hdr->ei_mag, ELFMAG, 4) != 0) {
		// printf("Error loading file 0x%x: Magic value mismatch\n", file);
		return 0xFFFFFFFF;
	}
	if(hdr->ei_version != EV_CURRENT || hdr->e_version != EV_CURRENT) {
		// printf("Error loading file 0x%x: Version mismatch\n", file);
		return 0xFFFFFFFF;
	}
	if(hdr->ei_class != ELFCLASS32) {
		// printf("Error loading file 0x%x: Class mismatch (supports only 32-bit files as of now)\n", file);
		return 0xFFFFFFFF;
	}
	if(hdr->ei_data != ELFDATA2LSB) {
		// printf("Error loading file 0x%x: Endianness mismatch (supports only little endian as of now)\n", file);
		return 0xFFFFFFFF;
	}
	if(hdr->e_type != ET_REL && hdr->e_type != ET_EXEC) {
		// printf("Error loading file 0x%x: Type mismatch (supports only REL and EXEC as of now)\n", file);
		return 0xFFFFFFFF;
	}
	if(hdr->e_type == ET_EXEC) {
		// printf("Warning: Executable loading isn't supported as of now\n");
	}
	if(hdr->e_machine != EM_386) {
		// printf("Error loading file 0x%x: Machine mismatch (supports only 386 as of now)\n", file);
		return 0xFFFFFFFF;
	}
	/* copy file to another location */
	if(mode != ELF32_MODE_EXPORT_SYMS) {
		file = malloc(size);
		memcpy(file, hdr, size);
		hdr = (elf32_hdr_t*) file;
		// printf("ELF file relocated to 0x%x\n", file);
		if(new_addr != NULL) *new_addr = (uintptr_t) file;
	}
	/* export symbols to global symbols table */
	if(mode & ELF32_MODE_BIT_EXPSYM) {
		for(uint32_t i = 1; i < hdr->e_shnum; i++) {
			elf32_shdr_t *shdr = elf32_section(hdr, i);
			if(shdr->sh_type == SHT_SYMTAB) {
				/* we've found a symbol table, dive deeper */
				for(uint32_t j = 1; j < shdr->sh_size / shdr->sh_entsize; j++) {
					elf32_sym_t *sym = (elf32_sym_t*) ((uintptr_t) hdr + shdr->sh_offset + j * sizeof(elf32_sym_t));
					if((ELF32_ST_TYPE(sym->st_info) == STT_FUNC || ELF32_ST_TYPE(sym->st_info) == STT_OBJECT || ELF32_ST_TYPE(sym->st_info) == STT_NOTYPE) && (ELF32_ST_BIND(sym->st_info) == STB_GLOBAL || ELF32_ST_BIND(sym->st_info) == STB_WEAK || ELF32_ST_BIND(sym->st_info) == STB_LOCAL) && sym->st_shndx != SHN_UNDEF) {
						elf32_shdr_t *strtab = elf32_section(hdr, shdr->sh_link);
						char *name = (char*) ((uintptr_t) hdr + strtab->sh_offset + sym->st_name);
						uint32_t ptr = (hdr->e_type == ET_EXEC) ? sym->st_value : elf32_get_symval(hdr, i, j);
						// printf("Symbol %s of type %u at 0x%x taking %u bytes discovered in section %s\n", name, ELF32_ST_TYPE(sym->st_info), ptr, sym->st_size, elf32_shstring(hdr, shdr->sh_name));
						elf32_add_sym(name, ELF32_ST_TYPE(sym->st_info), ELF32_ST_BIND(sym->st_info), ptr, sym->st_size);
					}
				}
			}
		}
	}
	if(hdr->e_type == ET_EXEC) return 0; // this is by far what we've supported
	/* load and relocate file */
	if(mode & ELF32_MODE_BIT_LOAD) {
		/* process BSS (aka elf_load_stage1()) */
		for(uint32_t i = 0; i < hdr->e_shnum; i++) {
			elf32_shdr_t *section = elf32_section(hdr, i);
			if(section->sh_type == SHT_NOBITS && section->sh_size != 0 && section->sh_flags & SHF_ALLOC) {
				section->sh_offset = (uintptr_t) calloc(section->sh_size, 1) - (uintptr_t) hdr;
				// printf("Allocated %u bytes of memory for section %s\n", section->sh_size, elf32_shstring(hdr, section->sh_name));
			}
		}
		/* perform relocation (aka elf_load_stage2()) */
		for(uint32_t i = 0; i < hdr->e_shnum; i++) {
			elf32_shdr_t *section = elf32_section(hdr, i);
			if(section->sh_type == SHT_REL) {
				for(uint32_t j = 0; j < section->sh_size / section->sh_entsize; j++) {
					elf32_rel_t *reltab = &((elf32_rel_t*)((uintptr_t) hdr + section->sh_offset))[j];
					if(elf32_reloc(hdr, reltab, section) == 0xFFFFFFFF) {
						// printf("Failed to relocate symbol\n");
						return 0xFFFFFFFF;
					}
				}
			}
		}
	}
	/* find init() */
	if(mode & ELF32_MODE_BIT_FIND_INIT) {
		uintptr_t ptr = elf32_find_sym(hdr, "init");
		if(ptr) {
			// printf("Found init() at 0x%x\n", ptr);
			return ptr;
		}
	}
	/* find main() */
	if(mode & ELF32_MODE_BIT_FIND_MAIN) {
		uintptr_t ptr = elf32_find_sym(hdr, "main");
		if(ptr) {
			// printf("Found main() at 0x%x\n", ptr);
			return ptr;
		}
	}
	return 0;
}

const char *elf32_addr2sym(uintptr_t addr, uint32_t *offset) {
	for(uint32_t i = 0; i < elf32_gstab_ents; i++) {
		if((elf32_gstab[i].ptr <= addr) && (addr < (elf32_gstab[i].ptr + elf32_gstab[i].size))) {
			if(offset != NULL) *offset = addr - elf32_gstab[i].ptr;
			return (const char *) &elf32_gstab[i].name;
		}
	}
	return NULL;
}
