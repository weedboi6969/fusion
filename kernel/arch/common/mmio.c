/*
 * Copyright (C) 2020 Succboy6000 (Thanh Vinh Nguyen).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <mmio.h>

void mmio_outb(uintptr_t addr, uint8_t val) {
    *(volatile uint8_t*) addr = val;
}

uint8_t mmio_inb(uintptr_t addr) {
    return *(volatile uint8_t*) addr;
}

uint16_t mmio_inw(uintptr_t addr) {
	return *(volatile uint16_t*) addr;
}

uint32_t mmio_inl(uintptr_t addr) {
	return *(volatile uint32_t*) addr;
}

void mmio_outw(uintptr_t addr, uint16_t val) {
	*(volatile uint16_t*) addr = val;
}

void mmio_outl(uintptr_t addr, uint32_t val) {
	*(volatile uint32_t*) addr = val;
}
