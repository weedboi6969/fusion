#include <utf8.h>
#include <string.h>

uint8_t utf8_bytes(uint8_t first) {
  // if((first & ~0x01) == 0xFE) return 6; // doesn't exist yet, just in case
  if((first & ~0x03) == 0xF8) return 5; // doesn't exist yet, just in case
  if((first & ~0x07) == 0xF0) return 4;
  else if((first & ~0x0F) == 0xE0) return 3;
  else if((first & ~0x1F) == 0xC0) return 2;
  else return 1;
}

uint32_t utf8_translate_32(uint8_t *c) {
  uint8_t bytes = utf8_bytes(c[0]);
  uint32_t ret = 0;
  switch(bytes) {
    case 1: ret = c[0] & 0x7F; break;
    case 2: ret = c[0] & 0x1F; break;
    case 3: ret = c[0] & 0x0F; break;
    case 4: ret = c[0] & 0x07; break;
    case 5: ret = c[0] & 0x03; break;
  }
  for(uint8_t i = 1; i < bytes; i++) ret = (ret << 6) | (c[i] & 0x3F);
  return ret;
}

uint8_t utf8_bytes32(uint32_t c) {
  if(c <= 0x7F) return 1;
  if(c <= 0x7FF) return 2;
  if(c <= 0xFFFF) return 3;
  if(c <= 0x10FFFF) return 4;
  return 5;
}

uint8_t utf8_translate_8(uint32_t c, uint8_t *out) {
  uint8_t bytes = utf8_bytes32(c);
  out[0] = 0;
  if(bytes == 1) out[0] = c;
  else {
    for(uint8_t i = 0; i < bytes; i++) out[0] = (out[0] >> 1) | (1 << 7);
    uint8_t bits = (bytes == 1) ? 7 : 11;
    if(bytes > 2) bits += 5 * (bytes - 2);
    uint8_t shift = bits - (7 + bytes);
    out[0] |= c >> shift;
    for(uint8_t i = 1; i < bytes; i++) {
      shift -= 6;
      out[i] = (1 << 7) | ((c >> shift) & 0x3F);
    }
  }
  return bytes;
}

size_t utf8_strlen(const uint8_t *s) {
  size_t ret = 0;
  uint32_t i = 0;
  while(s[i] != '\0') {
    ret++;
    i += utf8_bytes(s[i]);
  }
  return ret;
}

int utf8_strcmp(const char* str1, const char* str2) {
	uint8_t *p1 = (uint8_t *) str1;
	uint8_t *p2 = (uint8_t *) str2;
	uint32_t a = 0, b = 0;
  uint32_t i1 = 0, i2 = 0;
	while(1) {
		a = utf8_translate_32(&p1[i1]); b = utf8_translate_32(&p2[i2]);
    i1 += utf8_bytes(p1[i1]); i2 += utf8_bytes(p2[i2]);
		if(a != b || !a || !b) break;
	}
	return (int) (a - b);
}

int utf8_strncmp(const void* str1, const void* str2, size_t n) {
  uint8_t *p1 = (uint8_t *) str1;
	uint8_t *p2 = (uint8_t *) str2;
  uint32_t a = 0, b = 0;
  uint32_t i1 = 0, i2 = 0;
	for(uint32_t i = 0; i < n; i++) {
    a = utf8_translate_32(&p1[i1]); b = utf8_translate_32(&p2[i2]);
    i1 += utf8_bytes(p1[i1]); i2 += utf8_bytes(p2[i2]);
		if(a != b || !a || !b) break;
	}
	return (int) (a - b);
}

char* utf8_strcpy(char* dest, const char* src) {
	return strcpy(dest, src); // there's no differences between utf8_strcpy and strcpy (at least for now)
}
