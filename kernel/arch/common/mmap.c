/*
 * Copyright (C) 2020 Succboy6000 (Thanh Vinh Nguyen).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <mmap.h>
#include <pfa.h>
#include <stdlib.h>

mmap_entry_t *mmap = NULL;
uint32_t mmap_entries = 0;

void mmap_register(uintptr_t start, uintptr_t len, uint8_t flags) {
	mmap = realloc(mmap, sizeof(mmap_entry_t) * ++mmap_entries);
	mmap[mmap_entries - 1].start = start;
	mmap[mmap_entries - 1].len = len;
	mmap[mmap_entries - 1].flags = flags;
	if(flags == MMAP_RESERVED) {
		for(uintptr_t i = 0; i < len; i += 0x1000) {
			pfa_setframe((start + i) / 4096);
		}
	}
}

uint8_t mmap_status(uintptr_t addr) {
	for(uint32_t i = 0; i < mmap_entries; i++) {
		if(mmap[i].start <= addr && (mmap[i].start + mmap[i].len) >= addr) return mmap[i].flags;
	}
	return MMAP_UNDEF;
}
