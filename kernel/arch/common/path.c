/*
 * Copyright (C) 2020 Succboy6000 (Thanh Vinh Nguyen).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <path.h>
#include <string.h>
#include <assert.h>
#include <stdlib.h>
#include <stdio.h>

char path_cwd[1024] = "/"; // current working directory path

/* resolve absolute and relative path into proper absolute and store back into path */
static inline uint32_t lastnode(char **nodes, uint32_t start) {
  for(int32_t i = start - 1; i >= 0; i--) {
    if(strlen(nodes[i]) != 0) return i;
  }
  return 0;
}

char *path_resolve(char *path) {
  char *pathbuf = calloc(4096, 1); // temporary buffer for resolving
  if(path[0] == '/') strcpy(pathbuf, path); // absolute path, just copy straight into temp buffer
  else sprintf(pathbuf, "%s/%s", path_cwd, path); // relative path, append path to current working directory
  if(strlen(pathbuf) > 1 && pathbuf[strlen(pathbuf) - 1] == '/') pathbuf[strlen(pathbuf) - 1] = '\0'; // remove ending slash as it's redundant
  if(strlen(pathbuf) > 1) {
    /* resolve path */
    uint32_t cnt = 1, len = strlen(pathbuf);
    char **nodes = malloc(sizeof(char*));
    char dummy[1] = ""; // dummy string
    *nodes = &pathbuf[1];
    for(uint32_t i = 1; i < len; i++) {
      if(pathbuf[i] == '/') {
        pathbuf[i] = '\0';
        nodes = realloc(nodes, ++cnt * sizeof(char*));
        nodes[cnt - 1] = &pathbuf[i + 1];
      }
    }
    for(uint32_t i = 0; i < cnt; i++) {
      if(!strcmp(nodes[i], ".")) nodes[i] = dummy;
      else if(!strcmp(nodes[i], "..")) {
        nodes[i] = dummy;
        nodes[lastnode(nodes, i)] = dummy;
      }
    }
    char *pbuf2 = calloc(4096, 1);
    for(uint32_t i = 0; i < cnt; i++) {
      // printf("%u: %s (%u)\n", i, nodes[i], strlen(nodes[i]));
      if(strlen(nodes[i]) == 0) continue;
      pbuf2[strlen(pbuf2)] = '/';
      strcat(pbuf2, nodes[i]);
    }
    if(strlen(pbuf2) == 0) pbuf2[0] = '/';
    memcpy(pathbuf, pbuf2, strlen(pbuf2) + 1);
    free(pbuf2);
  }
  memcpy(path, pathbuf, strlen(pathbuf) + 1);
  free(pathbuf);
  return path;
}

char *path_setcwd(char *buf) {
  char *buf2 = malloc(strlen(path_cwd) + strlen(buf) + 2);
  strcpy(buf2, buf);
  strcpy(path_cwd, path_resolve(buf2));
  free(buf2);
  return buf;
}

char *path_getcwd(char *buf, uint32_t len) {
  return memcpy(buf, path_cwd, ((strlen(path_cwd) > len) ? len : strlen(path_cwd)) + 1);
}

vfs_node_t *path_getnode(char *path) {
  if(!strcmp(path, "/") || !strlen(path)) return vfs_root;
  char *buf2 = malloc(strlen(path_cwd) + strlen(path) + 2);
  strcpy(buf2, path);
  path_resolve(buf2); // now buf2 contains the absolute path to the node we need to find
  /* separate path to node names */
  uint32_t nodes = 0, len = strlen(buf2);
  for(uint32_t i = 0; i < len; i++) {
    if(buf2[i] == '/') {
      buf2[i] = 0;
      nodes++;
    }
  }
  char *t = &buf2[1]; // start at first node
  vfs_node_t *ret = vfs_root; // starting at root
  for(uint32_t i = 0; i < nodes; i++) {
    /* iterate through all the nodes */
    ret = vfs_finddir(ret, t);
    if(ret == NULL) return NULL; // cannot find node
    t = &t[strlen(t) + 1]; // next node
  }
  return ret;
}
