KERNEL_ARCH_COMMON_OBJS=\
$(ARCHDIR_COMMON)/stdio.o \
$(ARCHDIR_COMMON)/string.o \
$(ARCHDIR_COMMON)/stdlib.o \
$(ARCHDIR_COMMON)/timer.o \
$(ARCHDIR_COMMON)/pfa.o \
$(ARCHDIR_COMMON)/assert.o \
$(ARCHDIR_COMMON)/mmio.o \
$(ARCHDIR_COMMON)/framebuffer.o \
$(ARCHDIR_COMMON)/placement.o \
$(ARCHDIR_COMMON)/keyboard.o \
$(ARCHDIR_COMMON)/mouse.o \
$(ARCHDIR_COMMON)/vfs.o \
$(ARCHDIR_COMMON)/fat12.o \
$(ARCHDIR_COMMON)/utf8.o \
$(ARCHDIR_COMMON)/psf.o \
$(ARCHDIR_COMMON)/task.o \
$(ARCHDIR_COMMON)/tty.o \
$(ARCHDIR_COMMON)/devfs.o \
$(ARCHDIR_COMMON)/sync.o \
$(ARCHDIR_COMMON)/pcm.o \
$(ARCHDIR_COMMON)/libgcc.o \
$(ARCHDIR_COMMON)/mmap.o \
$(ARCHDIR_COMMON)/power.o \
$(ARCHDIR_COMMON)/elf32.o \
$(ARCHDIR_COMMON)/path.o \
$(ARCHDIR_COMMON)/uart.o
