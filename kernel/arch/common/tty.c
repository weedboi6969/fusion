/*
 * Copyright (C) 2020 Succboy6000 (Thanh Vinh Nguyen).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <tty.h>
#include <framebuffer.h>
#include <devfs.h>
#include <utf8.h>
#include <textmode.h>
#include <keyboard.h>
#include <stdio.h>

vfs_node_t *ttydev = NULL;

uint32_t tty_vfs_read(vfs_node_t *node, uint32_t offset, uint32_t size, uint8_t *buf) {
	(void) offset; // there's no offset for stream devices
	if(!devfs_isopen(node)) return 0; // we can't read from closed device
	uint32_t i = 0, orig_size = size;
	if(!size) size = 0xFFFFFFFF;
	while(i < size) {
		while(!kb_status());
		uint32_t c = kb_getkey();
		if(utf8_bytes(c) + i >= size) break;
		i += utf8_translate_8(c, (uint8_t*) &buf[i]);
		if(!orig_size) break; // if size = 0 then we can assume that the caller wanted us to return one block of data
	}
	return i;
}

uint32_t tty_vfs_write(vfs_node_t *node, uint32_t offset, uint32_t size, uint8_t *buf) {
	(void) offset; // there's no offset for stream devices
	if(!devfs_isopen(node)) return 0; // we can't write to closed device
	uint32_t i = 0;
	while(i < size) {
		uint32_t c = utf8_translate_32((uint8_t*) &buf[i]);
		text_putc(c);
		i += utf8_bytes(buf[i]);
	}
	return size;
}

int tty_vfs_init(void) {
	if(ttydev != NULL) return 0;
	ttydev = devfs_register("tty", (void*) tty_vfs_read, (void*) tty_vfs_write);
	return 0; // success
}
