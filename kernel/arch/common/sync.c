/*
 * Copyright (C) 2020 Succboy6000 (Thanh Vinh Nguyen).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <sync.h>
#include <stdlib.h>
#include <task.h>

#ifdef COMMON_MUTEX
void *mutex_create(void) {
	uint8_t *mutex = calloc(sizeof(uint8_t), 1);
	return mutex;
}

void mutex_destroy(void *mutex) {
	free(mutex);
}

int mutex_acquire(void *mutex) {
	uint8_t *t = (uint8_t*) mutex;
	if(*t) return -1; // not available
	else *t = 1; // acquire mutex
	return 0;
}

void mutex_release(void *mutex) {
	uint8_t *t = (uint8_t*) mutex;
	*t = 0;
}
#endif

#ifdef COMMON_SEMAPHORE
void *sem_create(void) {
	int *sem = calloc(sizeof(int), 1);
	return sem;
}

void sem_destroy(void *sem) {
	free(sem);
}

void sem_wait(void *sem) {
	int *t = (int*) sem;
	*t = *t - 1;
	if(*t < 0) task_yield(); // there's definitely a better way of doing this
}

void sem_signal(void *sem) {
	int *t = (int*) sem;
	*t = *t + 1;
	if(*t < 0) task_yield(); // there's definitely a better way of doing this
}
#endif

#ifdef COMMON_SPINLOCK
void *splk_create(void) {
	uint8_t *splk = calloc(sizeof(uint8_t), 1);
	return splk;
}

void splk_destroy(void *splk) {
	free(splk);
}

void splk_acquire(void *splk) {
	volatile uint8_t *t = (volatile uint8_t*) splk;
	while(!*t);
	*t = 1;
}

void splk_release(void *splk) {
	uint8_t *t = (uint8_t*) splk;
	*t = 0;
}
#endif
