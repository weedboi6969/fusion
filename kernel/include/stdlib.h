/*
 * Copyright (C) 2020 Succboy6000 (Thanh Vinh Nguyen).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef STDLIB_H
#define STDLIB_H

#include <stddef.h>
#include <stdint.h>

typedef struct {
	/* heap information type */
	uint32_t size; // cutrent heap size (excluding this struct, including all the headers and footers)
	uint32_t max_size; // the maximum size it can expand to
} __attribute__ ((packed)) heap_info_t;

typedef struct {
	/* heap block header */
	uint32_t in_use; // set to 1 if the block is in use
	uint32_t size; // block size (excluding header and footer)
} __attribute__ ((packed)) heap_header_t;

typedef struct {
	/* heap block footer */
	heap_header_t *header; // block's header
} __attribute__ ((packed)) heap_footer_t;

extern heap_info_t *heap_info;
extern uint32_t rand_seed;

int atoi(const char *str);
char* itoa(int num, char* str, uint8_t base);
char* itoa_unsigned(uint32_t num, char* str, uint8_t base);
void heap_init(uintptr_t addr, uintptr_t init_size, uintptr_t max_size);
void *malloc(size_t size);
void *malloc_ext(size_t size, uint32_t alignment, uintptr_t *physaddr);
void free(void *ptr);
void *calloc(size_t nitems, size_t size);
void *realloc(void *ptr, size_t size);
int abs(int i);
uint32_t rand(void);
void srand(uint32_t seed);
__attribute__ ((noreturn)) void exit(void);

#endif
