/*
 * Copyright (C) 2020 Succboy6000 (Thanh Vinh Nguyen).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef I386_PC_GDT_H
#define I386_PC_GDT_H

#include <stddef.h>
#include <stdint.h>

typedef struct {
	uint16_t limit_0_15; // limit 0:15
	uint16_t base_0_15; // base 0:15
	uint8_t base_16_23; // base 16:23
	uint8_t access; // access byte
	uint8_t limit_16_19 : 4; // limit 16:19
	uint8_t flags : 4; // flags
	uint8_t base_24_31; // base 24:31
} __attribute__ ((packed)) gdt_entry_t;

typedef struct {
	uint16_t size;
	uint32_t offset;
} __attribute__ ((packed)) gdtr_t;

void gdt_add_entry(uint8_t entry, uintptr_t base, uintptr_t limit, uint8_t access, uint8_t flags);
void gdt_init(void);

#endif
