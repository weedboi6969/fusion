/*
 * Copyright (C) 2020 Succboy6000 (Thanh Vinh Nguyen).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef I386_PC_IDT_H
#define I386_PC_IDT_H

#include <stddef.h>
#include <stdint.h>

typedef struct {
	uint16_t offset_0_15; // offset 0:15
	uint16_t selector; // code segment selector
	uint8_t zero; // set this to 0
	uint8_t type : 4; // gate type
	uint8_t attrib : 4; // attributes
	uint16_t offset_16_31; // offset 16:31
} __attribute__ ((packed)) idt_entry_t;

typedef struct {
	uint16_t size;
	uint32_t offset;
} __attribute__ ((packed)) idtr_t;

typedef struct {
	uint32_t edi, esi, ebp, esp, ebx, edx, ecx, eax;
} __attribute__ ((packed)) idt_regs_t;

#define IDT_TYPE_TASK		0x05
#define IDT_TYPE_INT		0x0E
#define IDT_TYPE_TRAP		0x0F

void idt_add_entry(uint8_t gate, uint8_t selector, uintptr_t offset, uint8_t type, uint8_t attrib);
void idt_init(void);
void idt_reboot(void);
void idt_add_handler(uint8_t vector, void (*handler)(uint8_t, idt_regs_t *));

#endif
